Для решения квалификационного задания мы использовали пакет `depthimage_to_laserscan`

Ссылка на видео с демонстрацией работы программы  https://yadi.sk/i/OpDx_A3B0qcGCA

Перейдите в папку *hackathon_kobuki* командой `cd hackathon_kobuki/`

Соберите докер `bash docker/simulator/build_docker.sh`

Запустите докер `bash docker/simulator/run_docker.sh`

Перейдите в директорию *simulator_ws* командой `cd simulator_ws/`

Скомпелируйте пакет `catkin_make`

Скачайте пакет навигации `sudo apt install ros-kinetic-turtlebot-navigation`

Выполните команду в консоли `roslaunch tb_gazebo turtletown_5.launch`

После этого в консоли выполните команду 
` 
rostopic pub /move_base_simple/goal geometry_msgs/PoseStamped "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: 'map'
pose:
  position:
    x: -8.577
    y: 6.299
    z: 0.0
  orientation:
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0" 
`
