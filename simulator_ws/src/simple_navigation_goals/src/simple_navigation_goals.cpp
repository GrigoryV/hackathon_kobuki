#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
double coord_x[2] = { -8.577, 0.0};
double coord_y[2] = {6.229, 0.0};
typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

int main(int argc, char** argv){
  ros::init(argc, argv, "simple_navigation_goals");
  ros::NodeHandle n;
  ros::Rate loop_rate(10);
  //tell the action client that we want to spin a thread by default
  MoveBaseClient ac("move_base", true);

  //wait for the action server to come up
  while(!ac.waitForServer(ros::Duration(5.0))){
    ROS_INFO("Waiting for the move_base action server to come up");
  }

  move_base_msgs::MoveBaseGoal goal;

  //we'll send a goal to the robot to move 1 meter forward
  goal.target_pose.header.frame_id = "map";
  goal.target_pose.pose.orientation.w = 1.0;
  int i = 0;
  bool f = true;
  //ac.waitForResult();
  while(ros::ok())
  {
  if((ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED) || f)
    {
    ROS_INFO("Hooray");
    f = false;
    i += 1;
    if( i > 1)
    {
    i = 0;
    }
    goal.target_pose.header.stamp = ros::Time::now();
    goal.target_pose.pose.position.x = coord_x[i];
    goal.target_pose.pose.position.y = coord_y[i];
    ROS_INFO("Sending goal");
    ac.sendGoal(goal);
    }
  
  ros::spinOnce();
  loop_rate.sleep();
  }
  return 0;
}
